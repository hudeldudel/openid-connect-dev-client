package com.nimbusds.openid.connect.client.vaadin;


import com.vaadin.server.AbstractClientConnector;
import com.vaadin.server.AbstractExtension;
import com.vaadin.ui.TextField;


/**
 * Integer field.
 */
public class IntegerField extends AbstractExtension {
	
	
	public static void extend(TextField field) {
		
		new IntegerField().extend((AbstractClientConnector) field);
	}
}