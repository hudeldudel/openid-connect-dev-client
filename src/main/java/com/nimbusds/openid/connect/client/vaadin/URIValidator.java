package com.nimbusds.openid.connect.client.vaadin;


import java.net.URI;

import com.vaadin.data.validator.AbstractStringValidator;


/**
 * URI validator.
 */
public final class URIValidator extends AbstractStringValidator {
	
	
	public URIValidator() {
		
		super("Invalid URI");
	}
	
	
	@Override
	protected boolean isValidValue(String s) {
		
		try {
			validate(s);
			
		} catch (InvalidValueException e) {
			
			return false;
		}
		
		return true;
	}
	
	
	@Override
	public void validate(final Object value)
		throws InvalidValueException {
		
		try {
			new URI((String)value);
			
		} catch (Exception e) {
			
			throw new InvalidValueException("Invalid URI");
		}
	}
}
